# BunnyBootstick

* Author:        dark_pyrro
* Version:       1.0
* Target:        Windows....ish
* Props:         Someone @ Hak5 Discord that asked a question that made me not being able to keep my fingers away from the keyboard
* Category:      Exfiltration (and possible other things, like.... infiltration, and stuff....)

## Requirements
Make sure that the DUCKY_LANG is set, either in the config.txt file of the Bunny or here in this script.
Using us as language because the OS on the Bunny Micro SD card uses that language by default.
Even though us is default language for the Bunny HID, it's still good practice to specify it.

## Description
Boots an OS from the Micro SD card on the Bunny.
Then interacts with the target using a pre-defined Ducky script payload.
Could really be almost anything, but in this particular piece of PoC code
it will get the Windows registry hives and exfiltrate them to the Bunny storage.
